import { DeletePostUseCase } from '../src/usecases/delete-post.usercase.js';

describe('Delete post', () => {
    it('should delete post', async () => {
        const postId = 1;
        const result = await DeletePostUseCase.execute(postId);
        expect(result).toEqual({});
    });
    });