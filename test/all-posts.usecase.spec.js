import { PostsRepository } from "../src/repositories/posts.repository";
import { AllPostsUseCase } from "../src/usecases/all-posts.usecase";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe("All posts use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get all posts", async () => {
    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          return POSTS;
        },
      };
    });

    const posts = await AllPostsUseCase.execute();

    expect(posts.length).toBe(100);

    for (let i = 0; i < posts.length; i++) {
      expect(posts[i].id).toBe(POSTS[i].id);
    }

    posts.forEach((post) => {
      expect(post.id).toBe(POSTS[post.id - 1].id);
      expect(post.title).toBe(POSTS[post.id - 1].title);
      expect(post.content).toBe(POSTS[post.id - 1].body);
    });
    expect(posts[0].content).toBe(POSTS[0].body);
  });
});
