import { CreatePostUseCase } from "../src/usecases/create-post.usecase";

describe("Create new post", () => {


  it("should get info about created post", async () => {
    const post = {
      title: "Test",
      content: "Test content",
    };
    const result = await CreatePostUseCase.execute(post);
    expect(result.title).toBe(post.title);
    expect(result.content).toBe(post.content);
  });
});