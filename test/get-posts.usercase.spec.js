import { PostsRepository } from "../src/repositories/posts.repository";

describe("Fetch not null posts", () => {
  it("should fetch not null posts", async () => {
    const repository = new PostsRepository();
    const posts = await repository.getAllPosts();

    expect(posts).not.toBeNull();
  });
});
