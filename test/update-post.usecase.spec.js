import { UpdatePostUseCase } from '../src/usecases/update-post.usecase.js';

describe('Update post', () => {
    it('should update post', async () => {
        const post = {
            id: 1,
            title: 'Test',
            content: 'Test content'
        };
        const result = await UpdatePostUseCase.execute(post);
        expect(result.title).toBe(post.title);
        expect(result.content).toBe(post.content);
    });
});