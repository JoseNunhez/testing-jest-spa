import { PostsRepository } from "../src/repositories/posts.repository";
import { OddPostsUseCase } from "../src/usecases/odd-posts.usecase";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe("Odd posts use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get all posts", async () => {
    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          return POSTS;
        },
      };
    });

    const posts = await OddPostsUseCase.execute();

    expect(posts.length).toBe(50);
    
    expect(posts.every((post) => post.id % 2 === 1)).toBe(true);
    expect(posts[0].title).toBe(POSTS[0].title);
    expect(posts[0].content).toBe(POSTS[0].body);
  });
});