import axios from "axios";

export class PostsRepository {
  async getAllPosts() {
    return await (
      await axios.get("https://jsonplaceholder.typicode.com/posts")
    ).data;
  }
}

export class CreatePostRepository {
  async createPost(post) {
    return await (
      await axios.post("https://jsonplaceholder.typicode.com/posts", post)
    ).data;
  }
}

export class DeletePostRepository {
  async deletePost(id) {
    return await (
      await axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
    ).data;
  }
}

export class UpdatePostRepository {
  async updatePost(post) {
    return await (
      await axios.put(`https://jsonplaceholder.typicode.com/posts/${post.id}`, post)
    ).data;
  }
}