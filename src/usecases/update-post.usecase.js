import { UpdatePostRepository } from "../repositories/posts.repository";

export class UpdatePostUseCase {
  static async execute(post) {
    const repository = new UpdatePostRepository();
    return await repository.updatePost(post);
  }
}
