import { Post } from "../model/post";
import { PostsRepository } from "../repositories/posts.repository";

export class OddPostsUseCase {
  static async execute() {
    const repository = new PostsRepository();
    const posts = await repository.getAllPosts();
    const oddPosts = posts.filter((post) => post.id % 2 === 1);
    return oddPosts.map(
        (post) =>
            new Post({
                id: post.id,
                title: post.title,
                content: post.body,
            })
    );
  }
}