import { DeletePostRepository} from "../repositories/posts.repository";

export class DeletePostUseCase {
  static async execute(postId) {
    const repository = new DeletePostRepository();
    return await repository.deletePost(postId);
  }
}