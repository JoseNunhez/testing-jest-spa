import { CreatePostRepository } from "../repositories/posts.repository";

export class CreatePostUseCase {
  static async execute(post) {
    const repository = new CreatePostRepository();
    return await repository.createPost(post);
  }
}

